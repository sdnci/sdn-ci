'''
Created on 03 set 2016

@author: gab
'''

from datetime import datetime
from gurobipy import *
import os
from pprint import pprint, pformat
from toposort import toposort
from collections import Counter, namedtuple
import sys


IDS = 'IDS'

'''
 - nodes_M: list of the machines (inside the substations, no switches)
 - nodes_N: all switches, also those inside substations
 - arcs: list of all edges, with capacity (biridirectional)
 - capacity: multidict() -> see pizzo code
 - streams: each object of the substation send traffic to the scada, and each scada server talks 
			with the IDS; it is a list of tuples (each tuple is a stream)
'''


def run_gurobi(nodes_M, nodes_N, arcs, capacity, streams):
	print str(datetime.now()) + " Start."

	assert set(reduce(lambda x, y: x + y, map(list, arcs))).issubset(set(nodes_N + nodes_M))
	# nodes specified in streams should be only in nodes list
	assert set(
		reduce(
			lambda x, y: x + y, map(lambda x: [x[0], x[1]], streams))).issubset(set(nodes_N + nodes_M))
	assert 'IDS' in set(nodes_M)

	dirarcs = []  # directed arcs, this will become a tuplelist
	c = {}  # capacity of directred links

	for i, j in arcs:
		c[i, j] = capacity[i, j]
		c[j, i] = capacity[i, j]
		dirarcs.append((i, j))
		dirarcs.append((j, i))
	dirarcs = tuplelist(dirarcs)  # tuplelist has been imported by gurobi

	# 	import collections
	# 	counter=collections.Counter(dirarcs)
	# 	pprint(counter.most_common(5))





	relevance = {}
	dirstreams = tuplelist()  # unidirectional streams (from, to, bandwidth, name)

	dupchecksigma1 = set()
	streams.sort()
	streams_c = Counter(map(lambda x: (min(x[0], x[1]), max(x[0], x[1])), streams))
	assert len(streams_c) == len(streams), "duplicated items \n%s" % pformat(
		[(v, n) for v, n in streams_c.items() if n > 1])

	print "# of streams:", len(streams)
	it = 0
	for n1, n2, b_fw, b_rev, rel_fw, rel_rev in streams:
		assert n1 != n2, "stream loop: %s <--> %s" % (n1, n2)
		sigmaFw = n1 + '->' + n2
		assert len(dirstreams.select(('*', '*', '*', sigmaFw))) == 0, "duplicated: %s" % sigmaFw
		assert not sigmaFw in dupchecksigma1, "iter. %d, fw, duplicated: %s" % (it, sigmaFw)
		dupchecksigma1.add(sigmaFw)
		dirstreams.append((n1, n2, b_fw, sigmaFw))

		sigmaRev = n2 + '->' + n1
		assert len(dirstreams.select(('*', '*', '*', sigmaRev))) == 0, "duplicated: %s" % sigmaRev
		assert not sigmaRev in dupchecksigma1, "iter. %d, rev, duplicated: %s" % (it, sigmaRev)
		dupchecksigma1.add(sigmaRev)
		dirstreams.append((n2, n1, b_rev, sigmaRev))

		relevance[sigmaFw] = rel_fw
		relevance[sigmaRev] = rel_rev
		it += 1

	print str(datetime.now()) + " Network created."

	# helper

	def neighors(v):
		return set([u for u, w in dirarcs.select('*', v)]) | set([w for u, w in dirarcs.select(v, '*')])

	########################################
	# GUROBI model setup
	m = Model('netflow')
	m.setAttr("ModelSense", -1)  # maximization
	m.params.timeLimit = 3600.0  # max one hour
	m.params.NumericFocus = 3   # highest numerical accuracy

	# GUROBI Variables creation

	print str(datetime.now()) + " starting defining model"
	print " vars: x and r..."
	# x[sigma,s,t] flow of sigma for each directed edge s->t
	x = {}
	r = {}

	# 	dupchecktriple = set()
	#  	dupchecknametail = set()
	dupchecksigma = set()
	for _s, _t, b, sigma in dirstreams:

		if sigma in dupchecksigma:
			print 'duplicated: ' + sigma
		dupchecksigma.add(sigma)

		for ss, tt in dirarcs:
			triple = (sigma, ss, tt)
			nametail = '%s__%s__%s' % triple

			# 			if triple in dupchecktriple:
			# 				print 'duplicated: '+str(triple)
			# 			dupchecktriple.add(triple)
			#
			# 			if nametail in dupchecknametail:
			# 				print 'duplicated: '+nametail
			# 			dupchecknametail.add(nametail)


			x[sigma, ss, tt] = m.addVar(
				vtype=GRB.BINARY, obj=-float(b) / c[ss, tt], name='x__' + nametail)

			r[sigma, ss, tt] = m.addVar(
				vtype=GRB.BINARY, obj=-float(b) / c[ss, tt], name='r__' + nametail)

	print "Expressions OutX, OutR, InX, InR, F, Fr."

	def outX(sigma, v):
		# s, t, b, _ = dirstreams.select(('*','*','*',sigma))
		return quicksum(x[sigma, ss, tt] for ss, tt in dirarcs.select(v, '*'))

	def inX(sigma, v):
		# s, t, b, _ = dirstreams.select(('*','*','*',sigma))
		return quicksum(x[sigma, ss, tt] for ss, tt in dirarcs.select('*', v))

	def outR(sigma, v):
		# s, t, b, _ = dirstreams.select(('*','*','*',sigma))
		return quicksum(r[sigma, ss, tt] for ss, tt in dirarcs.select(v, '*'))

	def inR(sigma, v):
		# s, t, b, _ = dirstreams.select(('*','*','*',sigma))
		return quicksum(r[sigma, ss, tt] for ss, tt in dirarcs.select('*', v))

	def fX(sigma, v):
		return outX(sigma, v) - inX(sigma, v)

	def fR(sigma, v):
		return outR(sigma, v) - inR(sigma, v)

	'''
	OutX = {}	#	how much stream actually exit from a vertex (gurobi expression)
	for s, t, b, sigma in dirstreams:
		for v in nodes_N + nodes_M:
			OutX[sigma, v] = quicksum(x[ sigma, ss, tt] for ss, tt in dirarcs.select(v, '*'))

	InX = {}	#	how much stream actually enter in a vertex (gurobi expression)
	for s, t, b, sigma in dirstreams:
		for v in nodes_N + nodes_M:
			InX[sigma, v] = quicksum(x[ sigma, ss, tt] for ss, tt in dirarcs.select('*', v))

	OutR = {}	#	how much replica stream actually exit from a vertex (gurobi expression)
	for s, t, b, sigma in dirstreams:
		for v in nodes_N + nodes_M:
			OutR[sigma, v] = quicksum(r[ sigma, ss, tt] for ss, tt in dirarcs.select(v, '*'))

	InR = {}	#	how much replica stream actually enter in a vertex (gurobi expression)
	for s, t, b, sigma in dirstreams:
		for v in nodes_N + nodes_M:
			InR[sigma, v] = quicksum(r[ sigma, ss, tt] for ss, tt in dirarcs.select('*', v))

	F = {}	# F[sigma, v] stream flow imbalance for each vertex and commodity (gurobi expression)
	Fr = {}	# Fr		replica stream flow imbalance
	for _s, _t, _b, sigma in dirstreams:
		for v in nodes_M + nodes_N:
			F[sigma, v] = OutX[sigma, v] - InX[sigma, v]
			Fr[sigma, v] = OutR[sigma, v] - InR[sigma, v]
	'''
	M = sum(2 * float(b) / c[ss, tt] for _s, _t, b, _sigma in dirstreams for ss, tt in dirarcs)
	print "M=", M, "(relevance factor for replication over fractional residual bandwidth)"

	repl = {}
	for s, t, b, sigma in dirstreams:
		repl[sigma] = m.addVar(
			vtype=GRB.BINARY, obj=M * relevance[sigma], name='isReplicated_%s' % (sigma))

	M2 = 2 * M * len(dirstreams)
	print "M2=", M2, "(relevance factor for routing over replication)"

	routed = {}
	for s, t, b, sigma in dirstreams:
		routed[sigma] = m.addVar(
			vtype=GRB.BINARY, obj=M2, name='isRouted_%s' % (sigma))

	m.update()

	print str(datetime.now()) + " Variables defined."

	# this is for correctly stating the objective function
	for s, t, b, sigma in dirstreams:
		m.addConstr(repl[sigma] == inR(sigma, IDS), name='linkObjFunc_replicated_%s' % (sigma))

	# this is for correctly stating the objective function
	for s, t, b, sigma in dirstreams:
		m.addConstr(routed[sigma] == outX(sigma, s), name='linkObjFunc_routed_%s' % (sigma))

	# constraints

	print str(datetime.now()) + " Constr: capacity..."
	# Arc capacity constraints
	for ss, tt in dirarcs:
		m.addConstr(
			quicksum(
				b * (x[sigma, ss, tt] + r[sigma, ss, tt])
				for _s, _t, b, sigma in dirstreams) <= c[ss, tt], 'capacity_%s_%s' % (ss, tt))
	'''
	 print str(datetime.now()) + " Constr: all critical streams should be routed..."
	 # impose all streams routed
	 for s, t, b, sigma in dirstreams:
		 m.addConstr(OutX[sigma, s] == 1, 'routed_source_%s' % (sigma))
		 m.addConstr(InX[sigma, t] == 1, 'routed_target_%s' % (sigma))
	'''

	print str(
		datetime.now()) + " Constr: optional routing streams routed[sigma]=source=target<=1 ...(deviation form paper)"
	for s, t, b, sigma in dirstreams:
		m.addConstr(outX(sigma, s) == inX(sigma, t), 'normal_sourceEqTarget_%s' % (sigma))
		m.addConstr(outX(sigma, s) <= 1, 'normal_max1_%s' % (sigma))
		m.addConstr(inX(sigma, s) == 0, 'normal_no_loop_source_%s' % (sigma))
		m.addConstr(outX(sigma, t) == 0, 'normal_no_loop_target_%s' % (sigma))

	print str(datetime.now()) + " Constr: flow conservation (normal)...."
	# normal flow conservation
	for s, t, _b, sigma in dirstreams:
		for v in set(nodes_N + nodes_M) - set([s, t]):
			m.addConstr(fX(sigma, v) == 0, 'consX_imb_%s_%s' % (v, sigma))

	# replica flow conservation
	print str(datetime.now()) + " Constr: replica flow conservation..."
	for s, t, _b, sigma in dirstreams:
		lasthops = set(ss for ss, tt in dirarcs.select('*', t))
		for v in set(nodes_N) - lasthops:
			m.addConstr(fR(sigma, v) == 0, 'consR_%s_%s' % (v, sigma))

	# replica stream starts from v only if v is the last hop of the selected path - see paper
	print str(datetime.now()) + " Constr: replicas starts only from last hop..."
	for s, t, _b, sigma in dirstreams:
		lasthops = set(ss for ss, tt in dirarcs.select('*', t))
		for v in lasthops:  # for all possible last hop of sigma
			m.addConstr(x[sigma, v, t] >= fR(sigma, v), 'replicaIfLastHop_%s_%s' % (v, sigma))

	print str(datetime.now()) + " Constr: IDS is not a source (no IDS loops)..."
	for s, t, _b, sigma in dirstreams:
		for ss, tt in dirarcs.select(IDS, '*'):
			m.addConstr(r[sigma, ss, tt] == 0, 'replicaNoIDSloops_%s_%s' % (sigma, tt))

	print str(datetime.now()) + " Constr: M nodes cannot switch (normal)..."
	for s, t, _b, sigma in dirstreams:
		# normal stream, nodes_M cannot switch
		for v in set(nodes_M) - set([s, t]):
			for ss, tt in dirarcs.select('*', v) + dirarcs.select(v, '*'):
				m.addConstr(x[sigma, ss, tt] == 0, 'MNodesNoSwitchX_%s_%s_%s_%s' % (v, sigma, ss, tt))

	# replica stream, nodes_M cannot switch
	print str(datetime.now()) + " Constr: M nodes cannot switch (replica)..."
	for s, t, _b, sigma in dirstreams:
		for v in set(nodes_M) - set([IDS]):
			for ss, tt in dirarcs.select('*', v) + dirarcs.select(v, '*'):
				m.addConstr(r[sigma, ss, tt] == 0, 'MNodesNoSwitchR_%s_%s_%s_%s' % (v, sigma, ss, tt))

	m.update()

	print str(datetime.now()) + " Model setup finished"
	'''
	print "writing of out.lp skipped"
	print os.getcwd()
	m.write('out.lp')
	'''
	# Compute optimal flow
	print str(datetime.now()) + " Starting optimization"

	m.optimize()

	print str(datetime.now()) + " Optimization finished"

	def recreateSequence(darcs):
		from toposort import toposort_flatten
		dependFrom = {}
		for u, v in darcs:
			dependFrom[v] = set([u])

		return toposort_flatten(dependFrom)
	def extractInvolvedArcs(x, sigma):
		arcs = []
		for i, j in dirarcs:
			if x[sigma, i, j] > 0:
				# print('r %s -> %s: %g' % (i, j, replica[sigma, i, j]))
				arcs.append((i, j))
		return arcs

	def cap(e):
		return c[e[0], e[1]]

	def occX(e):
		u = e[0]
		v = e[1]
		return sum(flow[sigma, u, v] * b for _s, _t, b, sigma in dirstreams)

	def occR(e):
		u = e[0]
		v = e[1]
		return sum(replica[sigma, u, v] * b for _s, _t, b, sigma in dirstreams)

	def occ(e):
		return occR(e) + occX(e)

	def occFrac(e):
		return float(occ(e)) / cap(e)
	# in the OptResult structure, both streams and edges are meant to be directed, both directions are reported
	# for non-routed or non-replicated streams, entry is None
	OptResult = namedtuple('OptResult', "ok stream2path stream2replpath edge2occ")
	result = OptResult( ok=(m.status == GRB.Status.OPTIMAL),
						stream2path={ },  
						stream2replpath = {}, 
						edge2occ= {}
						)
	print "strating extracting results"
	# Print flow
	if m.status in [GRB.Status.OPTIMAL, GRB.Status.TIME_LIMIT]:
		flow = m.getAttr('x', x)
		replica = m.getAttr('x', r)
		isReplicated = m.getAttr('x', repl)
		isRouted = m.getAttr('x', routed)
		print "attrs got"
		routedStreams = 0
		notRoutedStreams = 0
		replicatedStreams = 0
		notReplicatedStreams = 0
				
		for stream in dirstreams:
			sys.stdout.write('.'); sys.stdout.flush()
			s, t, _b, sigma = stream
			if not isRouted[sigma]:
				# print "NOT ROUTED:", sigma, _b
				notRoutedStreams += 1
				result.stream2path[stream] = None
				result.stream2replpath[stream] = None
				continue
			routedStreams += 1
			# print('\nPath for stream %s:' % sigma)
			
			darc = extractInvolvedArcs(flow, sigma)
			path = recreateSequence(darc)
			result.stream2path[stream] = path

			assert path[0] == s
			assert path[-1] == t
			# print path
			if not isReplicated[sigma]:
				notReplicatedStreams += 1
				result.stream2replpath[stream] = None
				continue
			replicatedStreams += 1

			darc = extractInvolvedArcs(replica, sigma)
			replicapath = recreateSequence(darc)
			assert replicapath[-1] == IDS
			assert replicapath[0] == path[-2]
			
			result.stream2replpath[stream] = replicapath

		result.edge2occ.update({ e: occ(e) for e in dirarcs })
		# print replicapath


		

		darcsByOccupation = [(occFrac(e), e) for e in dirarcs]
		darcsByOccupation.sort()


	 
			
		
		for occ , e in darcsByOccupation[-20:]:
			if occ == 0:
				continue
			print "%s->%s	cap=%d	 occ=%d (%%%.3f)	 (norm=%d , repl=%d)" % (
				e[0], e[1], cap(e), occX(e) + occR(e), float(occX(e) + occR(e)) / cap(e) * 100,
				occX(e), occR(e))
		

		print "total streams:", len(dirstreams)
		print "   not routed:", notRoutedStreams
		print "   routed:", routedStreams
		print "	  not replicated:", notReplicatedStreams
		print "	  replicated :", replicatedStreams
		
		return result

#! /usr/bin/env python

'''
Created on 19 ago 2016

@author: gab
'''

import sys
from creator import SubstationCreator, StreamCreator
from model.node import City, IDS
import networkx as nx
from topology_loader import TopologyLoader
import util
from collections import Counter, OrderedDict, namedtuple
from pprint import pprint, pformat
import math

# ------------------------- argument parsing
import argparse

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--noopt',
					action='store_true',
					default=False,
					help='skip gurobi optimization')
parser.add_argument('--only',
					default=None,
					type=str,
					help='run only on one topology')
parser.add_argument('--events',
					default=False,
					action='store_true',
					help='generate stream events')
parser.add_argument('--seed',
					default=1789,
					type=int,
					help='set the seed for the random generator')

args = parser.parse_args()
pprint(args)
# ------------------------- argument parsing END




KILO_BIT = 1024
#substationRequirement = 300000 * KILO_BIT


topos = {}  # The map containing all topos with substations. The map is: map<name, nx.Graph>
streams = OrderedDict()  # The map containing all streams per topo. map<topo_name, list(stream)>




def debug():
	names = list(topos.keys())
	names.sort()
	for topo in names:
		G = topos.get(topo)
		ncc = nx.number_connected_components(topos[topo])
		assert ncc == 1, "%s is has %d!=1 connected components" %(topo, ncc)
		num_of_city = 0
		num_of_substations = 0
		num_of_machines = 0
		num_of_edges = 0
		num_of_streams = 0
		num_switches = 0
		cities=[]
		
		minBn = 1e100
		maxBn = -1
		minSSteo = 1e100
		minSSact = 1e100
		maxSSteo = -1
		maxSSact = -1
		
		
		for node in G.nodes():
			if node.is_city():
				Bn = bandwidthNode(G, node)
				minBn = min( minBn, Bn )
				maxBn = max( maxBn, Bn )
				
				nsubst = math.floor(2./3*Bn/G.substationRequirement)
				
				minSSteo = min( minSSteo, nsubst )
				maxSSteo = max( maxSSteo, nsubst )
				
				
				
				cities.append(node)
				num_of_city += 1
				substations = node.get_substations()
				num_of_substations += len(substations)
				
				minSSact = min( minSSact, len(substations) )
				maxSSact = max( maxSSact, len(substations) )
				
				for substation in substations:
					machines = substation.get_machines()
					num_of_machines += len(machines)
					num_switches += len(substation.get_switches())

		
		citygraph = nx.subgraph(G, cities)
		cityedges = citygraph.edges(data='bw')
		maximum = max(map(lambda x: x[2], cityedges))
		minimum = min(map(lambda x: x[2], cityedges))
		num_of_edges = len(G.edges())
		num_of_streams = len(streams.get(topo))

		print 'Summary for topology %s:' % topo
		print '   Connected:', nx.is_connected(topos.get(topo))
		print '   Number of cities: %s' % str(num_of_city)
		print '   Number of edges among cities: %d' % len(citygraph.edges())
		print '   max bw for edges among cities: %.2e' % maximum
		print '   min bw for edges among cities: %.2e' % minimum
		print '   min Bn among cities: %.2e' % minBn
		print '   max Bn among cities: %.2e' % maxBn

		print '   substationRequriement: %.2e' % G.substationRequirement
		#print '   min number of substations among cities: %d (theoretic)' % minSSteo
		#print '   max number of substations among cities: %d (theoretic)' % maxSSteo
		print '   min number of substations among cities: %d (actual)' % minSSact
		print '   max number of substations among cities: %d (actual)' % maxSSact
		print '   Total number of substations: %d' % num_of_substations
		print '   ', sorted(map (lambda x: len(x.get_substations()), citygraph.nodes()),reverse=True)
		print '   Number of nodes total: %d' % len(G.nodes())
		print '		 machines: %d' % num_of_machines
		print '		 network: %d' % (num_of_city + num_switches)
		print '			 cities routers: %d' % num_of_city
		print '			 switches in substations: %d' % num_switches
		print '		 other (e.g. IDS): %d' % (len(G.nodes()) - num_of_city - num_of_machines - num_switches)
		print '   Number of edges: %s' % str(num_of_edges)
		print '   Number of streams: %s' % str(num_of_streams)


def flatten(l):
	return [item for sublist in l for item in sublist]


def generate_stream_events(topo, G):
		from numpy.random import randint, seed, exponential
		seed(args.seed)
		q = 0
		switches = []
		nodes = G.nodes()
		for n in nodes:
			if n.is_city():
				substs = n.get_substations()
				q += len(substs)
				for ss in substs:
					switches += ss.get_switches()

		users = {}
		for u in xrange(q):  # generate q users
			evList = []
			users[u] = (switches[randint(0, len(switches))], evList)
			t = 0
			cnum = 0
			while t < 10*60:  # 10 minutes
				nextTime = t + exponential(5 * 60)  # 5 minutes
				evList.append(('B', u, cnum, nextTime, nodes[randint(0, len(nodes))]))
				endsAt = nextTime + exponential(5 * 60 * 3)
				evList.append(('E', u, cnum, endsAt))
				t = nextTime
				cnum += 1
			evList.sort(lambda x, y: cmp(x[3], y[3]))

		userSwitches = {k: v[0] for k, v in users.iteritems()}
		events = flatten([v[1] for v in users.itervalues()])
		events.sort(lambda x, y: cmp(x[3], y[3]))
		# pprint(users)

		#pprint(events)
		#pprint(userSwitches)
		
		import pickle
		toPickle = (events, userSwitches)
		with open(topo + '.pkl','w') as f:
			pickle.dump(toPickle, f)
		
		
		file = open(topo + '.txt', 'w')
		for e in events:
		  state = e[0]
		  src = str(userSwitches.get(e[1]))
		  cnum = str(e[2])
		  time = str(e[3])
		  if state == 'B':
			dst = str(e[4])
			if state == 'B':
			  stream = '(' + state + ',' + src + ',' + cnum + ',' + time + ',' + dst + ')' + "\n"
			else:
			  stream = '(' + state + ',' + src + ',' + cnum + ',' + time + ')' + "\n"
		  file.write(stream)
		  file.flush()
		file.close() 

'''
Search into graph a node with a certain id
'''


def _get_node(graph, node):
	for n in graph.nodes():
		if n.get_id() == node:
			return n


'''
Get bw of the original graph by parsing the edge's attributes
'''


def _get_bw(data):
	# Try to get LinkSpeedRaw attribute
	bw = data.get('LinkSpeedRaw', None)
	if bw == None:
		# Try to look for LinkNote
		bw = data.get('LinkNote')
		if bw == None:
			bw = data.get('LinkLabel', None)
			if bw == None:
				try:
					bw = float(_parse_link_label(bw))
				except:
					bw = 1e9  # default to  1 Gbit/sec
		else:
			bw = _parse_link_node(bw)

	try:
		bw = float(bw)
	except:
		bw = 1e9  # default to  1 Gbit/sec
		
	if bw > 1e9: # no more than 1 Gbit/sec
		bw = 1e9
	return bw


def _parse_link_node(bw):
	# LinkNote has the form XX Mbps
	speed, unit, final_space = bw.split(' ')
	if unit.startswith('M'):
		new_bw = str(int(speed) * 1000000)
	return new_bw


def _parse_link_label(bw):
	# LinkLabel has the form xx-yyMbps
	low_speed, max_speed = bw.split('-')
	# Just take the max value
	if 'M' in max_speed:
		speed = max_speed.split('M')[0]
		new_bw = str(int(speed) * 1000000)
	elif 'G' in max_speed:
		speed = max_speed.split('G')[0]
		new_bw = str(float(speed) * 1000000000)
	return new_bw


'''
Get the size of a city. That value is computed by summing the bw of all edges incident on
a given city
'''


def _get_size(graph, city):
	size = 0
	for edge in graph.edges(data=True):
		src = edge[0]
		dst = edge[1]
		if city.get_id() == src.get_id() or city.get_id() == dst.get_id():
			size += float(edge[2].get('bw'))
	return float(size)


def _has_max_degree(graph):
	node = None
	max_degree = 0
	degree = nx.degree(graph)
	for d in degree:
		n = degree.get(d)
		if n > max_degree:
			node = d
	return node


'''
Creating cities starting from the original topology
'''


def create_cities(original_topos):
	for topo in original_topos.keys():
		graph = nx.Graph()
		values = original_topos.get(topo)

		# For each node, creates a city and add it to the new graph
		for node in values.nodes():
			# IDS also contains the control_room; the IDS is placed in the node that has the sum of
			# highest bw for the nodes incident on its.
			city = City(node)
			graph.add_node(city, label=str(city))

		# Adding edges
		for edge in values.edges(data=True):
			src = edge[0]
			dst = edge[1]
			data = edge[2]

			# Selecting src and dst from the graph
			g_src = _get_node(graph, src)
			g_dst = _get_node(graph, dst)

			# Get the link speed of this edge
			if topo == '0_Cesnet1993':
				bw = _get_bw(data) * 1e4
			else:
				bw = _get_bw(data)

			# Add the new edge
			graph.add_edge(g_src, g_dst, {'bw': bw})

		N = graph.nodes()
		maxN = None
		maxBw = -1
		for n in N:
			totbw = bandwidthNode(graph, n)
			if totbw > maxBw:
				maxN = n
				maxBw = totbw

		ids = IDS(maxN)
		graph.add_node(ids, label=str(ids))
		graph.add_edge(ids, maxN, {'bw': 1e12})

		graph.substationRequirement = max( 2./3*maxBw/10, 30000 * KILO_BIT)

		topos[topo] = graph
		
		
'''
This is the sum of the bandwidth of all incident edges of a node
'''

def bandwidthNode(graph, node):
	incidentEdges = graph.edges(node, data=True)
	Bnode = sum([  attr['bw'] for u,v, attr in incidentEdges if u.is_city() and v.is_city()])
	return Bnode





'''
Building cities by adding substations to them.
'''


def build_cities(s_creator):
	# For each graph, take a city and add it one or more substation(s)
	for topo in topos.keys():
		print 'Starting to build city in %s topology.' % topo
		if topo == '2_Agis':
			exponent = 1
		else:
			#exponent = 0.7
			exponent = 0.8
		
		graph = topos.get(topo)
		count = 1
		
		nodelist = map ( lambda n: (n, bandwidthNode(graph, n)), graph.nodes() )
		nodelist.sort(key=lambda x: x[1], reverse=True )
		
		i=0
		nsubstations0 = math.floor(2./3*nodelist[0][1]/graph.substationRequirement)
		for node, Bn in nodelist:
			if node.is_city():
				i+=1
				
				#nsubstations = int(max(1, min(nsubstations0/math.pow(i,0.5),  math.floor(2./3*Bn/graph.substationRequirement) )))
				nsubstations = int( max(1, nsubstations0/math.pow(i,exponent)) )
				print i,  nsubstations
				
				for n in xrange(nsubstations):
					substation = s_creator.new_substation(graph, node, count)
					node.add_substation(substation)
					switches = substation.get_switches()
					for s in switches:
						graph.add_edge(node, s, {'bw': 1e9})
					count +=1
					
				'''
				while (n+1)*graph.substationRequirement < 2./3*Bn:
					substation = s_creator.new_substation(node, count)
					node.add_substation(substation)
					switches = substation.get_switches()
					for s in switches:
						graph.add_edge(node, s, {'bw': 1e9})
					count +=1
					n += 1
				'''
					
					
				'''
				substation = s_creator.new_substation(node, count)
				node.add_substation(substation)
				# Add substation's nodes and edges to the graph
				graph.add_nodes_from(substation.get_substation_graph_nodes())
				graph.add_edges_from(substation.get_substation_graph_edges())
				# Add the edge between the substation and the city
				# graph.add_edge(node, substation.get_scada(), {'bw': 1e9})	# 1 Gbps link
				switches = substation.get_switches()
				for s in switches:
					graph.add_edge(node, s, {'bw': 1e9})
				
				# While the size of the substation is in the range 1/3N, 2/3N, continue to add
				while (substation.get_size() > _get_size(graph, node) / 3 and
							   substation.get_size() < 2 * _get_size(graph, node) / 3):

					count += 1
					substation = s_creator.new_substation(node, count)
					node.add_substation(substation)
					# Add the edge between the substation a nd the city
					# graph.add_edge(node, substation.get_scada(), {'bw': 1e9})	# 1 Gbps link
					switches = substation.get_switches()
					for s in switches:
						graph.add_edge(node, s, {'bw': 1e9})
				'''
		nx.write_graphml(graph, topo+"-ss.graphml", prettyprint=True)




def build_streams(st_creator):
	# For each city, take the substation and create stream to allow machines to send traffic
	for topo in topos:
		current = topos.get(topo)  # The graph with the topology
		for node in current.nodes():
			if node.is_city():
				substations = node.get_substations()
				for substation in substations:
					machines = substation.get_machines()
					for machine in machines:
						if machine == substation.get_scada():
							continue
						
						stream = st_creator.new_stream(
							machine, substation.get_scada(), machine.get_up_bw(), machine.get_down_bw(), 1, 1)
						streams.setdefault(topo, []).append(stream)
					
#					 stream = st_creator.new_stream(
#						 substation.get_scada(), main_scada , machine.get_up_bw(), machine.get_down_bw(), 1, 1)
#					 streams.setdefault(topo, []).append(stream)
#				 
					'''
					# The stream between the scada and the ids
					stream = st_creator.new_stream(
						substation.get_scada(), _get_ids(current.nodes()), substation.get_scada_up_bw(),
						substation.get_scada_down_bw(), 1, 1)
					streams.setdefault(topo, []).append(stream)
					'''


def _get_ids(nodes):
	for node in nodes:
		if node.is_ids():
			return node


def run_simulations():
	for topo in sorted(topos):
		#	for topo in ['Cesnet1993']:
		# Convert topos structure into node_M, node_N, arcs, capacity and streams; then run gurobi
		nodes_M = []
		nodes_N = []
		arcs = []
		capacity = {}
		t_streams = []

		print ''
		print '-------------------------------------------------------------'
		print 'Running simulation over topology %s.' % topo
		current = topos.get(topo)

		# import matplotlib.pyplot as plt
		# nx.draw(current)  # networkx draw()
		# plt.draw()
		for node in current.nodes():
			if node.is_city():
				nodes_N.append(str(node))
				substations = node.get_substations()
				for substation in substations:
					for m in substation.get_machines():
						nodes_M.append(str(m))  # Equivalent to nodes_M.extend(substation.get_machines())

					# Also add the scada server to the machines
					nodes_M.append(str(substation.get_scada()))

					for s in substation.get_switches():
						nodes_N.append(str(s))  # Equivalent to nodes_N.extend(substation.get_switches())
			elif isinstance(node, IDS):
				nodes_M.append('IDS')

		for edge in current.edges(data=True):
			arc = None
			# To avoid to have the id number in a list and the string IDS i another one
			if isinstance(edge[0], IDS):
				arc = ('IDS', str(edge[1]))
			elif isinstance(edge[1], IDS):
				arc = (str(edge[0]), 'IDS')
			else:
				arc = (str(edge[0]), str(edge[1]))

			cap = edge[2].get('bw')
			# assert type(cap) in [int, float]
			arcs.append(arc)
			capacity[arc] = float(cap)*0.95 # reserving 5% for standard streams

		for stream in streams.get(topo):
			t_streams.append(stream.get_stream_as_tuple())

		# check_streams(streams)

		# t_streams.sort() # make the following deterministic

		# t_streams_c = Counter(map(lambda x: (x[0], x[1]), t_streams))
		# 		pprint(nodes_M)
		# 		pprint(nodes_N)
		#
		result = util.run_gurobi(nodes_M, nodes_N, arcs, capacity, t_streams)
		if not result.ok:
			print "ERROR: gurobi does not provide a solution"
			sys.exit(1)
		
		tr = {str(obj): obj for obj in current.nodes()} 
		def translate(x): return tr[x]
		
		for e, o in result.edge2occ.items():
			u,v = map(translate, e)
			current.edge[u][v]['occ'] = o
			
			
		nx.write_graphml(current, topo+'_result.graphml', prettyprint=True)
		print "file", topo+'_result.graphml', "written "
		
		generate_stream_events(topo, current)
		print "file", topo+'.txt', "written "
		

'''
def check_streams(streams):
	print 'I am starting to check the streams'
	for topo in streams.keys():
		l_streams = streams.get(topo)
		for stream in l_streams:
			src = stream.get_src()
			dst = stream.get_dst()
			for s1 in l_streams:
				src1 = s1.get_src()
				dst1 = s1.get_dst()
				if src == dst1 and src1 == dst:
					print stream
'''
if __name__ == '__main__':
	t_loader = TopologyLoader()
	original_topos = t_loader.load_topologies(args.only)
	# Starting for topos, build cityes
	create_cities(original_topos)
	# The substation creator
	s_creator = SubstationCreator()
	# Starting to build cities
	build_cities(s_creator)
	# The stream creator
	st_creator = StreamCreator()
	# Build the streams
	build_streams(st_creator)
	debug()

	# generate stream events
	if args.events:
		generate_stream_events()

	# Run simulation
	if not args.noopt:
		run_simulations()

#!/usr/bin/env python


import math
import sys

import os
import re
from pprint import pprint
import pylab as pl
import numpy as np

import matplotlib
from matplotlib.ticker import FuncFormatter




def to_percent(y, position):
    # Ignore the passed in position. This has the effect of scaling the default
    # tick locations.
    s = str(100 * y)

    # The percent symbol needs escaping in latex
    if matplotlib.rcParams['text.usetex'] is True:
        return s + r'$\%$'
    else:
        return s + '%'

formatter = FuncFormatter(to_percent)


filenames = [ name for name in os.listdir('.') if re.match('.*data$', name) ]

data=[]
weight=[]
names=[]
for filename in filenames:
	print(filename)
	names.append(filename.replace(".data",""))
	with file(filename) as f:
		lines = f.readlines()
		floatlist=[]
		for d in lines:
			try: 
				floatlist.append( float(d) )
			except: pass
		weight.append(np.ones_like(floatlist)/len(floatlist))
		data.append(floatlist)

for i in xrange(len(data)):
	print("%s, %e, %e" % (names[i], min(data[i]), max(data[i])))



pl.figure(1)
n, bins, _ = pl.hist(data, 
	bins=np.logspace(6, 10, 20),  
	histtype='step',
	label=names, 
	rwidth=0.1,
	weights=weight)
#pl.hist(data)
pl.gca().set_xscale("log")
#pl.gca().yaxis.set_major_formatter(formatter)
pl.gca().set_xlabel("bandwidth")
pl.gca().set_ylabel("fraction of stream set with that bandwidth")
pl.legend(prop={'size': 20})
pl.savefig('density-steps.pdf', format='pdf')



#pl.figure(2)
#bins_mean = [0.5 * (bins[i] + bins[i+1]) for i in range(len(n))]
#pl.gca().set_yscale('log')
#pl.gca().set_xscale('log')
#pl.plot(bins_mean, n)
# pl.plot(bins_mean, n[1], c='r')
# pl.plot(bins_mean, n[2], c='y')
# pl.plot(bins_mean, n[3], c='g')
#pl.savefig('density-line.pdf', format='pdf')
print "done"


# def distribution(d, buckets):
#     min=1e999
#     max=-1e999
#     for x in d:
#         if x<min:
#             min=x
#         if x>max:
#             max=x
# 
#     delta=  (max-min)/buckets
#     dist= [0]*buckets
#     labels= [0]*buckets
# 
#     for i in xrange(buckets):
#         labels[i]=min+delta/2+i*delta
# 
#     for x in d:
#         if x==max:
#             i=buckets-1
#         else:
#             i=int(math.floor((x-min)/(max-min)*buckets))
#         dist[i]=dist[i]+1
# 
#     return dist, labels
# 
# def distributionLog(d, buckets):
#     min=float('inf')
#     max=float('-inf')
#     for x1 in d:
#         x=math.log10(x1)
#         if x<min:
#             min=x
#         if x>max:
#             max=x
#     delta=  (max-min)/buckets
# 
#     dist= [0]*buckets
#     labels= [0]*buckets
# 
#     for i in xrange(buckets):
#         labels[i]=math.pow(10,min+delta/2+i*delta)
# 
#     for x1 in d:
#         
#         x=math.log10(x1)
#         if x==max:
#             i=buckets-1
#         else:
#             i=int(math.floor((x-min)/(max-min)*buckets))
#         dist[i]=dist[i]+1
# 
#     return dist, labels


	

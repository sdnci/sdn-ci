'''
Created on 19 ago 2016

@author: gab
'''

import random
import os
import sys

import networkx as nx

class TopologyLoader(object):
	def __init__(self):
		self._topos = {}
		self._topos_with_capacity = {}

	def load_topologies(self, only=None):
		topo_folder = os.path.abspath('topos')
		topos = os.listdir(topo_folder)
		for topo in topos:
			if 'graphml' in topo:
				name = topo.split('.')[0]
				if not only or name == only:
					G = nx.read_graphml(os.path.join(topo_folder, topo))
					if G.__class__.__name__ == 'DiGraph':
						G = G.to_undirected()
					self._topos[name] = G
					ncc = nx.number_connected_components(self._topos[name])
					assert ncc == 1, "%s is has %d!=1 connected components" %(name, ncc)
		return self._topos
	
	def load_topologies_with_results(self):
		name = sys.argv[1]
		self._topos_with_capacity[name] = nx.read_graphml(name)
		return self._topos_with_capacity

	def build_traffic_matricies(self):
		for topo in self._topos:
			# Create traffic matrix 5x5
			count = 0
			while count < 5:
				nodes = topo.nodes()
				# Selecting two nodes, one for the destination and the other one for the destination
				src = random.choice(nodes)
				while True:
					dst = random.choice(nodes)
					if dst != src:
						break
				count += 1
				
			
			print 'SRC and DST:', src, dst
			
			
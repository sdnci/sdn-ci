# README #

This is a companion site for a paper submission. Details about the content of this repository and the authors are provided in the paper. 

# Dependencies

* Gurobi (see the http://www.gurobi.com/ for licensing and installation)
* python 2.7 with libraries
    * networkx
    * pprint
    * matplotlib
    * pylab
    * numpy
    * toposort
    * gurobipy (is installed with gurobi)

Tests were performed under Linux Ubuntu 14.04.

# How to run

To run the off-line solver execute 

./simulation.py --only 4_Uninett2010_fixed

where 4_Uninett2010_fixed is one of the topologies in directory topos with no file extension. It generates results.graphml (bandwidth occupation) files and .pkl and .txt files (describing standard streams for on-line solver)

To run the on-line solver execute

./online.py 1_AttMpls_result.graphml

where 1_AttMpls_result.graphml can be substituted with any of the result.graphml files. It automatically get from the name the right .pkl for the streams. It outputs the bandwidth assigned to all streams.
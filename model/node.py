'''
Created on 01 set 2016

@author: gab
'''

from abc import ABCMeta

'''
A generic node of the topology
'''

class Node(object):
	__metaclass__ = ABCMeta
	
	def __init__(self, name):
		self._name = name
		self._is_city = None

	def get_id(self):
		return self._name

	def is_city(self):
		return self._is_city

'''
An IDS node
'''
	
class IDS(Node):
	
	def __init__(self, name):
		Node.__init__(self, name)
		self._is_city = False
	
	def __str__(self):
		return 'IDS'

	def __repr__(self):
		return str(self)

	def is_ids(self):
		return True

'''
A city is a node of the original topology. Each city is a collection of substations.
'''

class City(Node):

	def __init__(self, name):
		Node.__init__(self, name) # Just the ID of the original node
		self._substations = []
		self._is_city = True
		
	def __str__(self):
		return 'City_%s' % self._name
	
	def __repr__(self):
		return self._name

	def is_ids(self):
		return False

	def get_substations(self):
		return self._substations
	
	def add_substation(self, substation):
		self._substations.append(substation)


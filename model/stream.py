__author__ = 'rob'


class Stream(object):
    def __init__(self, source, target, s2tband, t2sband, s2trel, t2srel, path, bw):
        self._source = source
        self._target = target
        self._s2tband = s2tband
        self._t2sband = t2sband
        self._s2trel = s2trel
        self._t2srel = t2srel
        self._path = path
        self._bw = bw
        self._cnum = 0
        self._replica = None
        self._original = None

    def __str__(self):
        return str(self._source) + ' ' + str(self._target) + ' ' + str(self._s2tband) + ' ' + str(
            self._t2sband) + ' ' + str(self._s2trel) + ' ' + str(self._t2srel)

    def __repr__(self, *args, **kwargs):
        return str(self)

    def get_stream_as_tuple(self):
        return (
            str(self._source),
            str(self._target),
            self._s2tband,
            self._t2sband,
            self._s2trel,
            self._t2srel
        )

    def get_src(self):
        return str(self._source)

    def get_dst(self):
        return str(self._target)

    def get_src_and_dst(self):
        return str(self._source) + ' ' + str(self._target)

    def set_path(self,path):
        self._path = path

    def get_path(self):
        return self._path

    def set_bw(self,bw):
        self._bw = bw

    def get_bw(self):
        return self._bw
    
    def get_cnum(self):
    		return self._cnum
    
    def set_cnum(self, cnum):
    	self._cnum = cnum

    def get_replica(self):
        return self._replica

    def set_replica(self, replica):
        self._replica = replica

    def get_original(self):
        return self._original

    def set_original(self, original):
        self._original = original


'''
Created on 01 set 2016

@author: gab
'''

import networkx as nx
from model.element import ScadaServer, Switch

'''
Each substation is a collection of single elements. A substation is represented by a nx.graph
'''

class Substation(object):

	def __init__(self, graph, city, sub_id):
		# The name of the substation
		self._name = str(city) + '_sub_' + str(sub_id)
		
		self._scada= None
		
		# The graph the elements of this substation are in
		self._ss_graph = graph
		# The list of all machines in this substation
		self._machines = []
		# The list of all switches
		self._switches = []
		
		# Create two switches with IDes 100 and 101 respectively; then add those switches to the root.
		# Each element added to this substation will be added to both the switches
		self._sw1 = Switch(self._name + '_sw_200')
		self._sw2 = Switch(self._name + '_sw_201')
		
		self._ss_graph.add_node(self._sw1, label=str(self._sw1))
		self._ss_graph.add_node(self._sw2, label=str(self._sw2))
		
		self._switches.append(self._sw1)
		self._switches.append(self._sw2)

	def get_name(self):
		return self._name
	
	def get_scada(self):
		return self._scada
	
	def get_substation_graph_nodes(self):
		return self._ss_graph.nodes()

	def get_substation_graph_edges(self):
		return self._ss_graph.edges(data=True)

	def get_switches(self):
		return self._switches
	
	def get_machines(self):
		return self._machines
	
	def add_scada(self, element):
		self._scada = element
		self._machines.append(element)
		self._ss_graph.add_node(element, label=str(element))
		self._ss_graph.add_edge(element, self._sw1, {'bw': 1e9})
		self._ss_graph.add_edge(element, self._sw2, {'bw': 1e9})
			
	def add_hmi(self, element):
		self._machines.append(element)
		self._ss_graph.add_node(element, label=str(element))
		self._ss_graph.add_edge(element, self._sw1, {'bw': 1e9})
		self._ss_graph.add_edge(element, self._sw2, {'bw': 1e9})
		
	def add_hdb(self, element):
		self._machines.append(element)
		self._ss_graph.add_node(element, label=str(element))
		self._ss_graph.add_edge(element, self._sw1, {'bw': 1e9})
		self._ss_graph.add_edge(element, self._sw2, {'bw': 1e9})
	
	def add_element(self, element):
		self._machines.append(element)
		self._ss_graph.add_node(element, label=str(element))
		self._ss_graph.add_edge(element, self._sw1, {'bw': 1e8})
		self._ss_graph.add_edge(element, self._sw2, {'bw': 1e8})
		# Add this element to the machienes' list

	def get_scada_up_bw(self):
		up_bw = 0
		for element in self._machines:
			up_bw += element.get_up_bw()
		return up_bw

	def get_scada_down_bw(self):
		down_bw = 0
		for element in self._machines:
			down_bw += element.get_down_bw()
		return down_bw

	def get_size(self):
		return 10000000 * len(self._ss_graph.edges())
		
'''
Created on 01 set 2016

@author: gab
'''

from abc import ABCMeta

'''
A single element. It has a name, and a uplink and downlink bandwidth
'''

KILO_BIT = 1024

class Element(object):
	__metaclass__ = ABCMeta

	def __init__(self, name):
		self._name = name
		self._up_bw = None
		self._down_bw = None
	
	def __str__(self):
		return self._name
	
	def __repr__(self):
		return self._name
	
	def get_id(self):
		return self._name

	def get_up_bw(self):
		return self._up_bw

	def get_down_bw(self):
		return self._down_bw
	
	def is_city(self):
		return False

	def is_ids(self):
		return False

class Switch(Element):
	def __init__(self, name):
		Element.__init__(self, name)


class ScadaServer(Element):
	def __init__(self, name):
		Element.__init__(self, 'ScadaServer' + name)


class VoltageMeter(Element):
	def __init__(self, name):
		Element.__init__(self, 'VoltageMeter' + name)
		self._up_bw = 100*KILO_BIT
		self._down_bw = 10*KILO_BIT


class CircuitSwitcher(Element):
	def __init__(self, name):
		Element.__init__(self, 'CircuitSwitcher' + name)
		self._up_bw = 1.5*KILO_BIT
		self._down_bw = 1.5*KILO_BIT


class Breaker(Element):
	def __init__(self, name):
		Element.__init__(self, 'Breaker' + name)
		self._up_bw = 1.5*KILO_BIT
		self._down_bw = 1.5*KILO_BIT


class CurrentMeter(Element):
	def __init__(self, name):
		Element.__init__(self, 'CurrentMeter' + name)
		self._up_bw = 100*KILO_BIT
		self._down_bw = 10*KILO_BIT


class PowerTransformer(Element):
	def __init__(self, name):
		Element.__init__(self, 'PowerTransformer' + name)
		self._up_bw = 500*KILO_BIT
		self._down_bw = 50*KILO_BIT


class HumanMachineInteraction(Element):
	def __init__(self, name):
		Element.__init__(self, 'HMI' + name)
		self._up_bw = 3000*KILO_BIT
		self._down_bw = 30000*KILO_BIT


class HistorianDB(Element):
	def __init__(self, name):
		Element.__init__(self, 'HistorianDB' + name)
		self._up_bw = 3000*KILO_BIT
		self._down_bw = 30000*KILO_BIT
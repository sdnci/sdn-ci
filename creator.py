'''
Created on 01 set 2016

@author: gab
'''
'''
This class has in charge the task of creating a substation formed by a SCADA server as a node
and a set of different elements.
'''

from model.element import VoltageMeter, CircuitSwitcher, Breaker, CurrentMeter, PowerTransformer, \
    HumanMachineInteraction, HistorianDB, ScadaServer
from model.stream import Stream
from model.substation import Substation


class SubstationCreator(object):
    def __init__(self):
        self._substation = None

    def new_substation(self, graph, city, sub_id):
        self._substation = Substation(graph, city, sub_id)

        count = 0
        # Create a new substation.
        while count < 2:
            element = VoltageMeter('_' + str(count) + '_' + self._substation.get_name())
            self._substation.add_element(element)
            count += 1

        count = 0
        while count < 2:
            element = CircuitSwitcher('_' + str(count) + '_' + self._substation.get_name())
            self._substation.add_element(element)
            count += 1

        count = 0
        while count < 2:
            element = Breaker('_' + str(count) + '_' + self._substation.get_name())
            self._substation.add_element(element)
            count += 1

        count = 0
        while count < 2:
            element = CurrentMeter('_' + str(count) + '_' + self._substation.get_name())
            self._substation.add_element(element)
            count += 1

        element = PowerTransformer('_' + str(count) + '_' + self._substation.get_name())
        self._substation.add_element(element)

        element = HumanMachineInteraction('_' + str(count) + '_' + self._substation.get_name())
        self._substation.add_hmi(element)

        element = HistorianDB('_' + str(count) + '_' + self._substation.get_name())
        self._substation.add_hdb(element)

        element = ScadaServer('_' + str(count) + '_' + self._substation.get_name())
        self._substation.add_scada(element)

        return self._substation


'''
This class has in charge the task of creating streams.
'''


class StreamCreator():
    def __init__(self):
        self._stream = None

    def new_stream(self, source, target, s2t_bw, t2s_bw, s2t_rel, t2s_rel):
        self._stream = Stream(source, target, s2t_bw, t2s_bw, s2t_rel, t2s_rel, None, None)
        return self._stream

#! /usr/bin/env python
from __future__ import print_function
from pprint import pprint

__author__ = 'rob'

import os
import networkx as nx
from topology_loader import TopologyLoader
from model.stream import Stream
import sys


def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)
    
    
class Online(object):
	def __init__(self, topo, new, ids, s_stream, c_stream):

		self._topo = topo
		self._new = new
		self._ids = ids
		self._s_stream = s_stream
		self._c_stream = c_stream


	def widestpath(self, topo, s, t):
		width = {}
		previous = {}

		for v in topo.nodes():  # inizializations
			width[v] = -float("inf")
			previous[v] = None

		width[s] = float("inf")  # width from s to s
		q = topo.nodes()

		while len(q) is not 0:  # main loop
			u = self.get_max_width(width, q)  # source node in first case
			q.remove(u)

			if width[u] == -float("inf"):  # all remaining nodes are inaccessible from s
				break
			if u is t:  # we reached the target
				break

			for v in topo.neighbors(u):
				alt = max(width[v], min(width[u], topo.get_edge_data(u, v)['new_c']))
				if alt > width[v]:
					width[v] = alt
					previous[v] = u

			path = [t]
			pre = previous[t]
			while pre != None:
				path.append(pre)
				pre = previous[path[-1]]

			path.reverse()

		return path

	def get_max_width(self, width, q):

		max_value = None
		max_node = None
		for n in q:
			if width[n] > max_value:
				max_value = width[n]
				max_node = n
		return max_node

	def get_all_neighbors_at_distance_i(self, topo, source, distance):

		neighbors = []
		paths = nx.single_source_shortest_path(topo, source, distance)
		for p in paths:
			if len(paths[p]) == distance + 1:
				neighbors.append(p)

		return neighbors


	def water_fill(self, topo, s_stream):

		to_assign = set([ (st.get_src(), st.get_dst(), st.get_cnum(), st.get_replica(), st.get_original()) for st in s_stream ])

		for u, v, d in topo.edges(data=True):
			nos = len(d['streams'])
			if nos>0:
				d['ce'] = d['res'] / nos
			else:
				d['ce'] = float('inf')
										
		bottleneck_edge, bottleneck_value = self.get_bottleneck_on_topo(topo)
		u,v = bottleneck_edge
		bottleneck_streams = topo[u][v]['streams']
		for s in bottleneck_streams:
			if s.get_replica():
				buddy = s.get_replica()
			else:
				buddy = s.get_original()

			st = (s.get_src(), s.get_dst(), s.get_cnum(), s.get_replica(), s.get_original())
			buddyt = (buddy.get_src(), buddy.get_dst(), buddy.get_cnum(), buddy.get_replica(), buddy.get_original())

			s.set_bw(bottleneck_value)
			buddy.set_bw(bottleneck_value)
			
			# these might be removed before if buddy goes through the same bottlenack
			try:
				to_assign.remove(st)
			except:
				pass
			
			try:
				to_assign.remove(buddyt)
			except:
				pass
				
			
	def get_bottleneck_on_topo(self, topo):

		min_ce = float("inf")
		min_e = None

		for e in topo.edges(data=True):
			if e[2]['ce'] < min_ce:
				min_ce = e[2]['ce']
				min_e = e

		return (min_e[0],min_e[1]), min_ce


	def stream_on_edge(self, topo, edge):

		streams = topo[edge[0]][edge[1]]['streams']

		return streams

	def get_bottlenek_on_path(self, topo, path):

		edges = self.compute_edges(path)
		min = float("inf")
		for e in edges:
			if topo[e[0]][e[1]]['new_c'] < min:
				min = topo[e[0]][e[1]]['new_c']

		return min

	def compute_edges(self, path):

		edges = []
		for i in range(0, len(path) - 1):
			e = (path[i], path[i + 1])
			edges.append(e)
		return edges



# topo=topology
# N = the network nodes in the topology
# topoN=topology restricted to N (network) nodes only
# new=new event (streams are passed one by one)
# ids=IDS
# s_stream=set of streams (standard streams generate by --events)
def main(topo, N, topoN, new, ids, stream_current):
	o = Online(topo, new, ids, stream_current, [])

	for u,v,d in topo.edges(data=True):
		d['res'] = d['bw'] - d['occ']
		d['streams'] = []
		topo[u][v]['new_c'] = d['res'] / (len(d['streams']) + 1)
	
	b_best = -float("inf")
	p_best = None
	q_best = None

	s = new.get_src()
	t = new.get_dst()

	t_switches = topo.neighbors(t)
	topoNt = topoN.copy()
	topoNt.add_node(t)
	topoNt.add_edges_from([(sw,t) for sw in t_switches])

	#distance = len(nx.shortest_path(topo_new_c, source=s, target=t))
	distance = 3
	neighbors = o.get_all_neighbors_at_distance_i(topoNt, t, distance)

	for n in neighbors:
		if n == 'IDS':
			continue
		
		so = []
		ot = []
		
		if s != n: so = o.widestpath(topo, s, n)
		od = o.widestpath(topo, n, ids)
		if t != n: ot = o.widestpath(topo, n, t)

		b = min(o.get_bottlenek_on_path(topo, ot), o.get_bottlenek_on_path(topo, so),
				o.get_bottlenek_on_path(topo, od))

		if b > b_best:
			b_best = b
			op = n
			p_best = so + ot[1:]
			q_best = od

	if b_best > 0:
		new.set_bw(b_best)
		new.set_path(p_best)
		replica = Stream(op, ids, 0, 0, 0, 0, q_best, b_best)
		replica.user = new.user

		new.set_replica(replica)
		replica.set_original(new)
		
		stream_current.append(new)
		stream_current.append(replica)
		
		for u,v in o.compute_edges(p_best):
			topo[u][v]['streams'].append(new)

		for u,v in o.compute_edges(q_best):
			topo[u][v]['streams'].append(replica)


# 		cont = 100
		o.water_fill(topo, stream_current)
# 		topo_fill = result[0]
# 		s_stream_fill = result[1]
# 		cont_fill = result[2]

# 		while cont_fill != 0:
# 			result = o.water_fill(topo_fill, s_stream_fill, cont_fill)
# 			topo_fill = result[0]
# 			s_stream_fill = result[1]
# 			cont_fill = result[2]


		return (p_best, op, q_best, b_best)


def _get_ids(nodes):
	for node in nodes:
		if node == 'IDS':
			return node


def load_streams(topo):
	stream_folder = os.path.abspath('.')
	streams = os.listdir(stream_folder)
	for s in streams:
		if s.endswith('txt'):
			name = topo.split('_')[1]
			name = name.replace(".graphml", "")
			if name in s:
				return s


def build_streams_list(streams):
	s_file = open(streams, 'r')
	l_streams = s_file.readlines()
	s_file.close()
	return l_streams

def build_streams_list_pkl(streams):
	import pickle
	pklname = streams.replace(".txt", ".pkl")
	with open(pklname) as f:
		r = pickle.load(f)
	return r

if __name__ == '__main__':
	t_loader = TopologyLoader()
	topos = t_loader.load_topologies_with_results()
	for topo in topos:
		streams_current = []
		
		eprint( 'Running online algorithm on topology', topo)
		graph = topos.get(topo)
		N = [ n for n in graph.nodes() if n.startswith("City_") ]
		graphN = graph.subgraph(N)
		ids = _get_ids(graph)
		
		
		streams = load_streams(topo)
		l_streams = build_streams_list(streams)
		events, userSwitches = build_streams_list_pkl(streams)
		
# 		for event in l_streams:
# 			event = event.replace("\n", "").replace('(', '').replace(')', '')
# 			state, src, cnum, time, dst = event.split(',')

		for event in events:
 			state = event[0]
 			
			if state == 'B':
				_, user, cnum, time, dst = event
				obj_stream = Stream(userSwitches[user], dst, 0, 0, 0, 0, None, 0)
				obj_stream.user = user
				obj_stream.set_cnum(cnum)
				r= main(graph, N, graphN, obj_stream, ids, streams_current)
				if r:
					p_best, op, q_best, b_best = r
					print(b_best)
				else:
					print(0)
			elif state == 'E':
				_, user, cnum, time = event
				for s in streams_current[:]:
					if s.user == user and s.get_cnum() == cnum and s.get_replica():  # select only the original
						streams_current.remove(s)
						streams_current.remove(s.get_replica()) 
							
						
					
#			 print("--------------------------")
#			 for s in streams_current:
#				 print s.get_bw()
#			 print("--------------------------")



		# main()
